import './startMenu.scss';


function StartMenu(data) {
  return (
    <div className="StartMenu">
      <div id="container_button_host_page">
        <button onClick={() => {
          data.setStep(1)}}>JOUER</button>
      </div>
    </div>
  );
}

export default StartMenu;