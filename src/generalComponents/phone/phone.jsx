import './phone.scss';
import {useState, useEffect} from 'react';

import {setFunction} from '../../globalFunction.js';

// Steps to display phone
let stepsDisp = [1, 5, 8, 10, 12];

// Phone comp
function Phone(data) {
    // Set folded
    const [folded, setFolded] = useState(true);
    const [pickUp, setPickup] = useState(false);
    setFunction('setPickup', setPickup);

    // Key actions
    const keyPressHandler = (event) => {
        if(event.key === 'Escape') { // Echap
            if(!folded) setFolded(true);
        };
    };
    // Warning on unload
    const unloadWarning = (event) => {
        let message = "Êtes vous sûr de vouloir fermer la page ?";
        event.returnValue = message;
        return message;
    }
    // Events
    useEffect(() => {
        document.addEventListener("keydown", keyPressHandler);
        window.addEventListener("beforeunload", unloadWarning);

        return function cleanup() {
            document.removeEventListener("keydown", keyPressHandler);
            window.removeEventListener("beforeunload", unloadWarning);
        };
    });


    // Data to display
    let messages = data.messages;
    let messagesDiv = [];
    if(messages !== null) {
        for (let i in messages) {
            let msg = messages[i];

            let sender = msg.sender
            let className="phone-message"
            if (sender==="Moi") {
                className += " sender"
            } else {
                className += " receiver"
            }
            messagesDiv.push(<div key={i} id={"phoneMessage_"+i} className={className}>
                <span className="emitter">{sender} :</span>
                <span>{msg.content}</span> 
            </div>)
        }
    } else {
        
    }


    // Functions 
    function foldPhone() {
        setFolded(true);
        setPickup(false);

        if(data.step === 1) { // Passage de message à suite
            data.setStep(2);
        } else if(data.step === 5) {
            data.setStep(6);
        } else if(data.step === 8) {
            data.setStep(9);
        } else if(data.step === 10) {
            data.setStep(11);
        } else if(data.step === 12) {
            data.setStep(13);
        }
    }

    
    // Render Phone
    if(stepsDisp.includes(data.step)) {
        if(folded) {
            if(pickUp) { // Need to pick up phone
                return (
                    <div id="PickUpPhone" onClick={() => {setFolded(false);}}>
                        <div id="arrowPart">
                            <span className='arrowDiv'>ᐱ</span>
                            <span className='arrowDiv'>ᐱ</span>
                            <span className='arrowDiv'>ᐱ</span>
                        </div>
                        <div id="imgPart"></div>
                    </div>
                );
            } else { // Phone folded but no need to pick up
                return (
                    <div 
                        id="FoldedPhone" 
                        onClick={() => {setFolded(false);}}
                    ></div>
                );
            }
        } else {
            return (
                <div id="Phone">
                    <div id="phoneScreen">
                        <div id="phoneTop">
                            <div id="notch"></div>

                            <div id="arrowCloseDiv" onClick={() => {foldPhone();}}>
                                <span className='arrowDiv'>ᐯ</span>
                                <span className='arrowDiv'>ᐯ</span>
                                <span className='arrowDiv'>ᐯ</span>
                            </div>
                        </div>

                        <div id="phoneContent">
                            <div id="messages">
                                {messagesDiv}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
} export default Phone;