import './transition.scss';

// Phone comp
function TextTransition(data) {
    // Timeout to skip
    setTimeout(() => {
        data.setStep(data.step + 1);
    }, 4000);

    // Render tr
    return (
        <div id="Transition">
            <span>{data.text}</span>
        </div>
    );
} export default TextTransition;