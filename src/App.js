import './App.scss';

// Import react comps
import {useState} from 'react';

import TopBar from "./topBar/topBar";

// General components
import Phone from '../src/generalComponents/phone/phone.jsx';
import StartMenu from './generalComponents/startMenu/startMenu.js';
import Transition from './generalComponents/transition/transition.jsx';
import TextTransition from './generalComponents/transition/textTransition.jsx';

// Game components
import Association from './gamesComponents/association/association.jsx';
import Boite from './gamesComponents/boite/boite.js';
import Minigame from './gamesComponents/minigame/minigame.jsx';
import Chambre from './gamesComponents/chambre/chambre.jsx';


// Messages for phone
let messages = [
  null, 
  [
    {sender: 'Lucas', content: 'Salut ! On sort ce soir, tu t’en souviens ?! RDV ce soir au bar, puis on bouge en boîte.'},
    {sender: 'Moi', content: 'T’inquiètes, A tout’'},
  ], 
  null, 
  null, 
  null,
  [
    {sender: 'Lucas', content: "Alors, t'as conclus hier 😂?"},
    {sender: 'Moi', content: "Ouais mais je crois pas qu'on se soit protégés ... Qu'est ce que je fais ... ?"},
    {sender: 'Lucas', content: "T'as des symptomes ? Tu devrais chercher sur internet"},
  ],
  null,
  null,
  [
    {sender: 'Moi', content: "J'ai cherché, je sais pas quoi faire ..."},
    {sender: 'Lucas', content: "Appelle Sida Info Services, ils pourront t'aider !"},
    {sender: 'Moi', content: "J'ai appelé, ils m'ont dit d'aller aux urgences"}
  ],
  null,
  [
    {sender: 'Moi', content: "Les médecins m'ont donné un TPE, ça permet d'empêcher le VIH de se développer"},
    {sender: 'Lucas', content: "Ah zut ! Tu comptes faire quoi après ?"},
    {sender: 'Moi', content: "Il va falloir que je me fasse dépister rapidement"}
  ],
  null,
  [
    {sender: 'Moi', content: "Bon, je me suis fait dépister, j'ai une Hépatite B ..."},
    {sender: 'Lucas', content: "Merde .. Tu as pensé à prévenir la personne avec qui tu as eu la relation ?"},
    {sender: 'Moi', content: "Tu aurais son numéro ?"},
    {sender: 'Lucas', content: "Non, mais tu dois pouvoir le retrouver, elle m’a dit qu’il y avait sa date de naissance dans son 06 (avec les 4 chiffres de l'année). Elle fêtait ses 18ans"},
    {sender: 'Moi', content: "C'est quel genre d'info ??"},
    {sender: 'Lucas', content: "Apparemment elle était capricorne et elle se ventait d’être née le même jour que Vanessa Paradis. J’en sais pas plus désolé. Dépêches toi de la prévenir !"},
  ]
];



function App() {
  // Step of game
  const [step, setStep] = useState(0);


  // Content of page
  let compSteps = [
    <StartMenu setStep={setStep}/>,
    <Transition/>,
    <Boite step={step} setStep={setStep}/>,
    <Minigame step={step} setStep={setStep}/>,
    <TextTransition step={step} setStep={setStep} text="Le courant passe bien, vous ne finirez pas votre nuit dans la solitude ..."/>,
    <Transition/>,
    <TextTransition step={step} setStep={setStep} text="Vous cherchez sur internet les symptômes des différentes IST ..."/>,
    <Association step={step} setStep={setStep}/>,
    <Transition/>,
    <TextTransition step={step} setStep={setStep} text="Aux urgences ..."/>,
    <Transition/>,
    <TextTransition step={step} setStep={setStep} text="Une semaine plus tard ... Vous allez vous faire dépister"/>,
    <Transition/>,
    <Chambre step={step} setStep={setStep}/>,    
    <TextTransition step={step - 1} setStep={setStep} text="Bravo, vous avez réussi à prévenir votre partenaire, vous allez pouvoir vous soigner !"/>
  ];
  let contentPage = compSteps[step];
  let msg = messages[step];


  // Render
  return (
    <div className="App">
      <TopBar/>
      
      <div id="Game">
        {contentPage}
        
        <Phone step={step} messages={msg} setStep={setStep}/>
      </div>
    </div>
  );
}

export default App;
