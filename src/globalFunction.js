let functions = {};

function setFunction(name, callback) {
    functions[name] = callback;
}

exports.setFunction = setFunction;

function getFunction(name) {
    return functions[name];
}

exports.getFunction = getFunction;
