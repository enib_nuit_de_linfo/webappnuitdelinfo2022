import './dropdownContent.scss';

function DropdownContent(data) {
  return (
    <div className="dropdownContent">
      <div className="dropdownButton">ACCUEIL</div>
      <div className="dropdownButton">ACCESSIBILITÉ</div>
      <div className="dropdownButton">A PROPOS DE NOUS</div>
    </div>
  );
}

export default DropdownContent;