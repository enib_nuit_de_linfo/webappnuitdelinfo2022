import './burgerMenu.scss';
import {useState} from 'react'
import DropdownContent from "./dropdownContent/dropdownContent" 

function BurgerMenu(data) {
  const [opened, setOpened] = useState(false);

  let dropdown;
  if (opened) {
    dropdown = <DropdownContent/>
  }

  return (
    <div className="burgerMenu">
      <img onClick={()=>{setOpened(!opened)}} alt="burgerLogo" id="burgerLogo" src="burgerMenu.png" style={{filter:data.dayMode?"invert(0%)":"invert(100%)"}}/>
      {dropdown}
    </div>
  );
}

export default BurgerMenu;