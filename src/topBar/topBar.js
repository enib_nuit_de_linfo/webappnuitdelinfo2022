import './topBar.scss';
import BurgerMenu from './burgerMenu/burgerMenu'
import {useState} from 'react'
const nightLogo = "/night.png"
const dayLogo = "/day.png"
const gameDayLogo = "/logoWhiteMode.png"
const gameDarkLogo = "/logoDarkMode.png"


function TopBar() {
  const [mode, setMode] = useState("day")

  function switchMode() {
    let newMode = mode === "day" ? "night" : "day";
    document.documentElement.setAttribute('data-theme', newMode);
    setMode(newMode)
  }

  let dayMode = mode==="day"

  return (
    <div className="topBar">
      <img alt="logo" id="gameLogo" src={dayMode ? gameDayLogo : gameDarkLogo}/>
      <div id="themeSlider" onClick={switchMode}>
          <img alt="" src={dayMode ? nightLogo : dayLogo } style={{float: (dayMode ? "left" : "right")}}/>
      </div>
      <BurgerMenu dayMode={dayMode}/>
    </div>
  );
}

export default TopBar;