import React, { useEffect, useRef, useState } from "react";
import './minigame.scss';

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
class Block {
  constructor(data) {
    // data format: { x: int, y: int, w: int, h: int, dy: int }
    this.data = data;
  }

  setData(data) {
    this.data = data;
    this.data.w = this.data.type === 1 ? this.data.w / 1.5 : this.data.w;
    this.data.h = this.data.type === 1 ? this.data.h / 1.5 : this.data.h;
    this.data.dx_mul = Math.floor(Math.random() * 100) >= 50 ? -1 : 1;
    this.data.dx = 1;
  }

  draw(ctx) {
    ctx.beginPath();
    if (this.data.type === 0) {
      ctx.rect(this.data.x, this.data.y, this.data.w, this.data.h);
    } else if (this.data.type === 1) {
      ctx.arc(this.data.x, this.data.y, this.data.w, 0, 2 * Math.PI);
    }
    ctx.fillStyle = this.data.type === 0 ? "#ff4757" : "#2ed573";
    ctx.fill();
    ctx.closePath();

    ctx.beginPath();
    ctx.font = "25px Arial";
    ctx.fillStyle = "#000000";
    ctx.textAlign = "center";
    let strokeTextX =
      this.data.type === 1
        ? this.data.x + this.data.text.length * 0.25
        : this.data.x + this.data.w / 2;
    let strokeTextY =
      this.data.type === 1 ? this.data.y : this.data.y + this.data.h / 2;
    ctx.fillText(this.data.text, strokeTextX, strokeTextY);
    ctx.closePath();
  }

  update(ctx) {
    if (this.data.x + this.data.dx > ctx.canvas.width - this.data.w) {
      this.data.dx *= -1;
    }
    if (this.data.x - this.data.dx < this.data.w) {
      this.data.dx *= -1;
    }
    if (this.data.y + this.data.dy > ctx.canvas.height + this.data.h) {
      return { ev: "floor" };
    }
    this.data.x +=
      this.data.dx * Math.floor(Math.random() * 100) >= 15
        ? -this.data.dx_mul
        : this.data.dx_mul;
    this.data.y += this.data.dy;
    return { ev: "none" };
  }
}
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
class Player {
  constructor(_x, _y, width, height, _dx) {
    this.pos = { x: _x, y: _y };
    this.velo = { dx: _dx };
    this.rect = { w: width, h: height };
    this.sensor = { right: false, left: false };
    this.drawing = new Image();
    this.drawing.src = "./lui.png";
    this.candraw = false;
    this.drawing.onload = function () {
      this.candraw = true;
    };
  }

  draw(ctx) {
    ctx.drawImage(
      this.drawing,
      this.pos.x,
      this.pos.y,
      this.rect.w,
      this.rect.h
    );
  }

  keysHandler(type, key) {
    if (key === "Right" || key === "ArrowRight") {
      this.sensor.right = type === "down";
    } else if (key === "Left" || key === "ArrowLeft") {
      this.sensor.left = type === "down";
    }
  }

  update(ctx) {
    if (this.sensor.right) {
      this.pos.x = Math.min(
        this.pos.x + this.velo.dx,
        ctx.canvas.width - this.rect.w
      );
    } else if (this.sensor.left) {
      this.pos.x = Math.max(this.pos.x - this.velo.dx, 0);
    }
  }
}
///////////////////////////////////////////////////////////////

const words_1 = [
  "Blaguer",
  "Sourire",
  "Charmer",
  "Observation",
  "Interessant",
];

const words_2 = [
  "Sexisme",
  "Beauf",
  "Lourd",
  "Violent",
  "Mechant",
  "Insultant",
  "Harcelant",
  "Attouchement",
];

const Minigame = (data) => {
  const canvasRef = useRef(null);
  const [gameCompleted, setGameCompleted] = useState(false);

  function drawTopbar(ctx, progress) {
    ctx.beginPath();
    ctx.rect(0, 0, ctx.canvas.width, 25);
    ctx.fillStyle = "#dfe4ea";
    ctx.fill();
    ctx.closePath();

    ctx.beginPath();
    ctx.rect(0, 0, (progress * ctx.canvas.width) / 100, 25);
    ctx.fillStyle = "#2ed573";
    ctx.fill();
    ctx.closePath();

    ctx.beginPath();
    ctx.font = "25px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.textAlign = "center";
    ctx.strokeText(progress + "%", ctx.canvas.width / 2, 21);
    ctx.closePath();
  }


  useEffect(() => {
    if (gameCompleted) return;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    canvas.height = window.innerHeight * 0.85;

    const blocks = [];
    //const pool1 = new Pool(100, { x: 0, y: 0, w: 0, h: 0, dy: 0 });

    function presetBlock(block) {
      let tmp = Math.floor(Math.random() * 2);
      block.setData({
        x: Math.floor(Math.random() * ctx.canvas.width - 10),
        y: Math.floor(Math.random() * -45),
        w: 50 * (1 + Math.random() * 1.7),
        h: 50 * (1 + Math.random() * 1.7),
        dy: 1 + Math.floor(Math.random() * 4),
        type: tmp,
        text:
          tmp === 1
            ? words_1[Math.floor(Math.random() * words_1.length)]
            : words_2[Math.floor(Math.random() * words_2.length)],
      });
    }

    for (let i = 0; i < 5; i++) {
      blocks[i] = new Block({});
      presetBlock(blocks[i]);
    }

    // The player
    const player1 = new Player(
      (canvas.width - 25) / 2, // X
      ctx.canvas.height - 55, // Y
      25, // W
      55, // H
      5 // dx
    );

    //Progress
    let progress = 0; 
    setGameCompleted(false);

    function intersects(circle, pl) {
      let r = pl.rect.w;
      let circleDistance = {};
      circleDistance.x = Math.abs(circle.x - pl.pos.x);
      circleDistance.y = Math.abs(circle.y - pl.pos.y);

      if (circleDistance.x > pl.rect.w / 2 + r) {
        return false;
      }
      if (circleDistance.y > pl.rect.h / 2 + r) {
        return false;
      }

      if (circleDistance.x <= pl.rect.w / 2) {
        return true;
      }
      if (circleDistance.y <= pl.rect.h / 2) {
        return true;
      }

      let cornerDistance_sq =
        (circleDistance.x - pl.rect.w / 2) ** 2 +
        (circleDistance.y - pl.rect.h / 2) ** 2;

      return cornerDistance_sq <= r ** 2;
    }

    function collisionDetection() {
      for (let e = 0; e < blocks.length; e++) {
        const ene = blocks[e];
        if (ene.data.type === 1 && intersects(ene.data, player1)) {
          console.log("COLLISION CIRCLE");
          presetBlock(ene);
          progress += 5;
        } else if (
          ene.data.type === 0 &&
          player1.pos.x + player1.rect.w / 2 > ene.data.x &&
          player1.pos.x + player1.rect.w / 2 < ene.data.x + ene.data.w &&
          player1.pos.y - player1.rect.h / 2 > ene.data.y &&
          player1.pos.y - player1.rect.h / 2 < ene.data.y + ene.data.h
        ) {
          console.log("COLLISION");
          presetBlock(ene);
          progress -= 10;
        }
      }
    }

    function draw() {
        if (gameCompleted) return;
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      blocks.forEach((ele) => ele.draw(ctx));
      player1.draw(ctx);
      drawTopbar(ctx, progress);
      if (progress >= 100) setGameCompleted(true); 
      if (progress > 100) progress = 100;
      if (progress < 0) progress = 0;

      blocks.forEach((ele) => {
        let resp = ele.update(ctx);
        if (resp.ev === "floor") {
          presetBlock(ele);
        }
      });
      player1.update(ctx);
      collisionDetection();
    }

    function keyDownHandler(e) {
      player1.keysHandler("down", e.key);
    }

    function keyUpHandler(e) {
      player1.keysHandler("up", e.key);
    }

    function handleMove(evt) {
      evt.preventDefault();
      //player1.velo.dx = 0;
      let bcr = canvas.getBoundingClientRect();
      player1.pos.x = evt.changedTouches[0].clientX - bcr.left;
    }

    document.addEventListener("keydown", keyDownHandler, false);
    document.addEventListener("keyup", keyUpHandler, false);
    canvas.addEventListener("touchmove", handleMove);

    let drawInter = setInterval(draw, 10);

    return () => {
        clearInterval(drawInter);
    };
  }, [gameCompleted]);


  if(gameCompleted) {
    data.setStep(data.step+1);
  } else {
    return (
      <div id="Minigame">
        <canvas width={window.innerWidth} ref={canvasRef} />
      </div>
    );
  }
};

export default Minigame;
