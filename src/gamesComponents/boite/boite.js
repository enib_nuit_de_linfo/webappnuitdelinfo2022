import './boite.scss';
import {useState} from 'react';

let story = {
    "page0": {
        "typeSlide":"question",
        "question":"Avec qui souhaites-tu engager la discussion ?",
        "choix1":"Dylan",
        "choix2":"Léa",
        "next1":1,
        "next2":1,
        "image":"../../../public/BOITE_DE_NUIT.png"
    },
    "page1": {
        "typeSlide":"question",
        "question":"Que faire ?",
        "choix1":"Demander des conseils à mon ami",
        "choix2":"Aller voir les symptomes sur internet",
        "next1":2,
        "next2":2,
        "image":"../../../public/BOITE_DE_NUIT.png"
    },
    "page2":{
        "typeSlide":"texte",
        "texte":"Du Texte",
        "next":3,
        "image":"../../../public/BOITE_DE_NUIT.png"
    },
    "page3": {
        "typeSlide":"question",
        "question":"Que faire ?",
        "choix1":"Demander des conseils à mon ami",
        "choix2":"Aller voir les symptomes sur internet",
        "next1":1,
        "next2":1,
        "image":"../../../public/BRETAGNE.jpg"
    }
};
let index=0;
let currentSlide=0;
let nextSlide=0;
let position='middle';


// Phone comp
function Boite(data) {
    // Set state
    function choicesMade(value){
        nextSlide=story["page"+currentSlide]["next"+value];
        if(document.getElementById('choix'+value).classList.contains('active')){
            document.getElementById('choix'+value).classList.remove('active')
            nextSlide=0;
        }
        else{
            document.getElementById('choix'+value).classList.add('active')
        }
        if (value==1){
            if(document.getElementById('choix2').classList.contains('active')){
                document.getElementById('choix2').classList.remove('active');
            }
        }
        else{
            if(document.getElementById('choix1').classList.contains('active')){
                document.getElementById('choix1').classList.remove('active');
            }
        }
        // isthereachoice=true;
    }
    function validate(){
        if(nextSlide==0){
            document.getElementById("error").innerHTML="Vous n'avez fait aucun choix";
            move();
        } else {
            console.log('active')
            document.getElementById("error").innerHTML="<br>";
            document.getElementById("valider").classList.add("active");
        }
    }
    function move(){
        if (position=='right'){
            document.getElementById("valider").classList="left";
            position='left';
        }
        else{
            document.getElementById("valider").classList="right";
            position='right';
        }
    }
    function returnToMiddle(){
        if(position=='right' || position=='left'){
            position='middle';
            document.getElementById("valider").classList="";
        }
    }
    function devalidate(){
        if(document.getElementById("valider").classList.contains("active")){
            document.getElementById("valider").classList.remove("active");
        }
    }

    // Render tr
    return (
        <div id="Boite">
            <div id="container_button_host_page">
                <div id="choiceSlide" className="slide">
                    <div id="question">
                        <span>{story['page0']["question"]}</span>
                    </div>
                    <div id="answers">
                        <div id="choix1" className="bouton_choix" onClick={() => {choicesMade(1)}}>
                            <span>{story["page0"]["choix1"]}</span>
                        </div>
                        <div id="choix2" className="bouton_choix" onClick={() => {choicesMade(2)}}>
                            <span>{story["page0"]["choix2"]}</span>
                        </div>
                    </div>
                    <div id="validerSection" onClick={() => {
                        data.setStep(data.step + 1);
                    }} onMouseLeave={returnToMiddle}>
                        <div id="valider" onMouseOver={validate} onMouseLeave={devalidate}>VALIDER</div>
                        <div id="error"><br/></div>
                    </div>
                </div>
            </div>
        </div>
    );
} export default Boite;