import './association.scss';
import React from 'react';

// Data lists
const textList = require('./assocText.json');
let diseaseList = textList.diseases;
let possibilities = textList.consequencies;
let answers = textList.valids;
let foundDis = [];
let foundPoss = [];


// Association comp
export default class Association extends React.Component {
    constructor(data) {
        super(data);

        this.state = {
            selDisease: null,
            selPoss: []
        }

        // Step function
        this.step = data.step;
        this.setStep = data.setStep;

        // Functions
        this.renderDiseases = this.renderDiseases.bind(this);
        this.renderPossib = this.renderPossib.bind(this);

        this.selectDisease = this.selectDisease.bind(this);
        this.addPoss = this.addPoss.bind(this);

        this.validChoice = this.validChoice.bind(this);
    }

    // SELECT DATA
    selectDisease(index) {
        let sel = this.state.selDisease;
        
        if(sel === index) {
            this.setState({selDisease: null});
        } else {
            this.setState({selDisease: index});
        }
    }
    addPoss(index) {
        let listPoss = this.state.selPoss;

        if(!listPoss.includes(index)) {
            listPoss.push(index);
        } else {
            listPoss.splice(listPoss.indexOf(index), 1);
        }

        this.setState({selPoss: listPoss});
    }


    // EVENTS 
    validChoice() {
        let selDis = this.state.selDisease;
        let selPos = this.state.selPoss;
        
        // Test if good answer
        if(selPos.every((val) => answers[selDis].includes(val))) {
            foundDis.push(selDis);
            foundPoss.push(...selPos);
        }

        if(foundDis.length === diseaseList.length) {
            this.setStep(this.step+1);
        }


        // Deselect all
        this.setState({selDisease: null, selPoss: []});
    }


    // RENDER PARTS
    renderDiseases() {
        // Render diseases list
        let elemsDis = [];
        let sel = this.state.selDisease;

        for(let i = 0; i < diseaseList.length; i++) {
            if(!foundDis.includes(i)) {
                elemsDis.push(
                    <div 
                        className={"assocElement" + (sel === i ? ' selected' : '')}
                        key={`disease_${i}`} 
                        onClick={() => {this.selectDisease(i);}}>
                        <span>{diseaseList[i]}</span>
                    </div>
                );
            }
        }

        return elemsDis;
    }
    renderPossib() {
        // Render poss list 
        let elemsPoss = [];
        let sel = this.state.selPoss;

        for(let i = 0; i < possibilities.length; i++) {
            if(!foundPoss.includes(i)) {
                elemsPoss.push(
                    <div 
                        className={"assocElement" + (sel.includes(i) ? ' selected' : '')}
                        key={`possDis_${i}`}
                        onClick={() => {this.addPoss(i);}}>
                        <span>{possibilities[i]}</span>
                    </div>
                );
            }
        }

        return elemsPoss;
    }


    // RENDER 
    render() {
        // Rend part
        let renderDis = this.renderDiseases();
        let renderPoss = this.renderPossib();

        // Enable
        let enableBtn = this.state.selDisease !== null && this.state.selPoss.length > 0;

        // Render Assoc
        return (
            <div id="Association">
                <div className='sideAssoc' id="leftAssoc">
                    {renderDis}
                </div>

                <div className='sideAssoc' id="rightAssoc">
                    {renderPoss}
                </div>

                <div id="validDivAssoc">
                    <button 
                        disabled={!enableBtn}
                        onClick={() => {
                            this.validChoice();
                        }}>VALIDER</button>
                </div>
            </div>
        );
    }
}