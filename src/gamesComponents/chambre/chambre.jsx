import './chambre.scss';
import {useState} from 'react';

// Phone comp
function Chambre(data) {
    // Input
    const [inputVal, setVal] = useState("");

    // Clic on btn
    function verifLe06() {
        if(inputVal === "0622122004") { // Valid
            data.setStep(data.step + 1);
        } else { // Nope frérot
            setVal('');
        }
    }

    // Enable
    let enableBtn = inputVal.length === 10;

    // Render tr
    return (
        <div id="Chambre">
            <div id="inputDivEnd">
                <span>Entrez le numéro de tel :</span>
                <div id="inputEnd">
                    <input
                        placeholder='Entrez le numéro'
                        value={inputVal}
                        onChange={(event) => {
                            setVal(event.target.value)
                        }}
                    >
                    </input>

                    <button 
                        disabled={!enableBtn}
                        onClick={verifLe06}
                    >VALIDER</button>
                </div>
            </div>
        </div>
    );
} export default Chambre;