# Nuit de l'info 2022

## Présentation de la nuit de l'[info](https://www.nuitdelinfo.com) :

La Nuit de l’Info est une compétition nationale qui réunit étudiants, enseignants et entreprises pour travailler ensemble sur le développement d’une application web.

La Nuit se déroule tous les ans, du premier jeudi du mois de décembre, coucher du soleil, jusqu'au lever du soleil le lendemain matin.

Les participants ont la durée d'une nuit pour proposer, implémenter et packager une application Web 2.0.

## Présentation de l'[ENIB](https://www.enib.fr) :

Créé en 1961, l’École Nationale d’Ingénieurs de Brest (ENIB) est une grande école publique d'ingénieurs installée à Brest. 

Habilitée par la Commission des titres d’ingénieur (CTI) et labellisée EUR-ACE (label européen de qualité), l'ENIB forme en 5 ans des ingénieurs généralistes à finalité professionnelle dans les domaines de l’électronique, de l’informatique et de la mécatronique.

## Notre projet : Don't Get It 

Le sujet de cette année portait sur la réalisation d'un site prévention du VIH.

Nous avons donc choisi de réaliser ce projet en y apportant la réponse à 5 défis supplémentaires :

- Accessible à tous
- Submit me if you can
- To Git or not to Git 
- Vos régions ont du talent
- Escape Game 
 
Bien que nous n'étudions pas ces matières en cours, nous avons choisi les technologies JavaScript et ReactJS.

Au travers d'un scénario narratif, nous suivons l'avancée d'une personne qui est à risque d'une IST et tente de découvrir des bonnes pratiques.

## Lancement du projet :
- Installer les modules avec `npm i`
- Lancer la WebApp avec `npm start` pour une version peu optimisée
- Lancer la WebApp avec `npm run build`, puis `serve -s build` pour une version optimisée